package generate

import (
	"fmt"
	"github.com/golang/protobuf/protoc-gen-go/generator"
	"io"
	"modeltools/conf"
	"modeltools/dbtools"
	"os"
	"strings"
)

func Genertate(tableNames ...string)  {
	tableNamesStr := ""
	for _, name := range tableNames {
		if tableNamesStr != "" {
			tableNamesStr += ","
		}
		tableNamesStr += "'"+name+"'"
	}
	tables := getTables(tableNamesStr) //生成所有表信息
	//tables := getTables("admin_info","video_info") //生成指定表信息，可变参数可传入过个表名
	for _,table := range tables {
		fields := getFields(table.Name)
		generateModel(table,fields)
	}
}
//获取表信息
func getTables(tableNames string) []Table {
	db := dbtools.GetMysqlDb()
	var tables []Table
	if tableNames == "" {
		db.Raw("SELECT TABLE_NAME as Name,TABLE_COMMENT as Comment FROM information_schema.TABLES WHERE table_schema='"+conf.MasterDbConfig.DbName+"';").Find(&tables)
	}else{
		db.Raw("SELECT TABLE_NAME as Name,TABLE_COMMENT as Comment FROM information_schema.TABLES WHERE TABLE_NAME IN ("+tableNames+") AND table_schema='"+conf.MasterDbConfig.DbName+"';").Find(&tables)
	}
	return tables
}
//获取所有字段信息
func getFields(tableName string) []Field {
	db := dbtools.GetMysqlDb()
	var fields []Field
	db.Raw("show FULL COLUMNS from "+tableName+";").Find(&fields)
	return fields
}


//生成Model
func generateModel(table Table, fields []Field)  {
	//导包
	packages:=""
	importPackage:="import ("+packages+")"
	content := "package models\n\n"+importPackage+" \n\n"
	//表注释
	if len(table.Comment) > 0 {
		content += "// "+table.Comment+"\n"
	}
	content += "type "+generator.CamelCase(table.Name)+" struct {\n"
	//生成字段
	for _, field := range fields {
		fieldName := generator.CamelCase(field.Field)
		fieldJson := getFieldJson(field)
		fieldType := getFiledType(field)
		fieldComment := getFieldComment(field)
		content += "	"+fieldName+" "+fieldType+" `"+fieldJson+"` "+fieldComment+"\n"
	}
	content += "}"

	filename := conf.ModelPath+generator.CamelCase(table.Name)+".go"
	var f *os.File
	var err error
	if checkFileIsExist(filename){
		if !conf.ModelReplace {
			fmt.Println(generator.CamelCase(table.Name)+" 已存在，需删除才能重新生成...")
			return
		}
		f, err = os.OpenFile(filename, os.O_WRONLY|os.O_TRUNC, 0666) //打开文件
		if err != nil {
			panic(err)
		}
	}else{
		f, err = os.Create(filename)
		if err != nil {
			panic(err)
		}
	}
	defer f.Close()
	_, err = io.WriteString(f, content)
	if err != nil {
		panic(err)
	}else{
		fmt.Println(generator.CamelCase(table.Name)+" 已生成...")
	}
}
//获取字段类型
func getFiledType(field Field)  string{
	typeArr := strings.Split(field.Type,"(")
	switch typeArr[0] {
	case "int","integer","mediumint","bit","year","smallint","tinyint":
		return "int"
	case "bigint":
		return "int64"
	case "decimal","double","float","real","numeric":
		return "float32"
	case "timestamp","datetime","time":
		return "time.Time"
	default:
		return "string"
	}
}
//获取字段json描述
func getFieldJson(field Field) string {
	return `json:"`+CaseCamel(field.Field)+`"`
}
// 下划线写法转为驼峰写法
func CaseCamel(name string) string {
	name = strings.Replace(name, "_", " ", -1)
	name = strings.Title(name)
	return strings.Replace(name, " ", "", -1)
}
//获取字段说明
func getFieldComment(field Field) string{
	if len(field.Comment) > 0 {
		return "// "+field.Comment
	}
	return ""
}
//检查文件是否存在
func checkFileIsExist(filename string) bool {
	var exist = true
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		exist = false
	}
	return exist
}